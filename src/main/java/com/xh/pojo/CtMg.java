package com.xh.pojo;

/*
 * @author：Zero度
 * @2020/12/20 21:21
 * 文件说明：网站主要信息框架内容
 * */
public class CtMg {

    private Integer cateId;
    private String cateName;
    private Integer indexMenuId;
    private String indexMenuName;

    public CtMg() {
    }

    public CtMg(Integer cateId, String cateName, Integer indexMenuId, String indexMenuName) {
        this.cateId = cateId;
        this.cateName = cateName;
        this.indexMenuId = indexMenuId;
        this.indexMenuName = indexMenuName;
    }

    public Integer getCateId() {
        return cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public Integer getIndexMenuId() {
        return indexMenuId;
    }

    public void setIndexMenuId(Integer indexMenuId) {
        this.indexMenuId = indexMenuId;
    }

    public String getIndexMenuName() {
        return indexMenuName;
    }

    public void setIndexMenuName(String indexMenuName) {
        this.indexMenuName = indexMenuName;
    }

    @Override
    public String toString() {
        return "CtMg{" +
                "cateId=" + cateId +
                ", cateName='" + cateName + '\'' +
                ", indexMenuId=" + indexMenuId +
                ", indexMenuName='" + indexMenuName + '\'' +
                '}';
    }
}
