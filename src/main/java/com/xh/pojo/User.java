package com.xh.pojo;

import com.alibaba.fastjson.JSON;

import java.lang.annotation.Target;

/*
 * @author：Zero度
 * @2020/11/14 18:07
 * 文件说明：用户表信息
 * */
public class User {
    /*
    * 特殊处理的用户id
    * */
    private String uid;

    /*
    * 用户名
    * */
    private String userName;

    /*
    * 密码
    * */
    private String password;

    /**
     * 盐值
     */
    private String salt;

    /*
    * 身份：0 管理、3 游客、5 普通用户
    * */
    private String role;

    /*
     * 邮箱
     * */
    private String mail;

    /*
    * 权限
    * */
    private String authority;

    public User() {
    }

    public User(String uid, String userName, String password, String salt, String role, String mail, String authority) {
        this.uid = uid;
        this.userName = userName;
        this.password = password;
        this.salt = salt;
        this.role = role;
        this.mail = mail;
        this.authority = authority;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid='" + uid + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", role='" + role + '\'' +
                ", mail='" + mail + '\'' +
                ", authority='" + authority + '\'' +
                '}';
    }
}
