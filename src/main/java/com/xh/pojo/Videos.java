package com.xh.pojo;

/*
 * @author：Zero度
 * @2021/2/3 22:00
 * 文件说明：
 * */
public class Videos {
    private String vid;
    private String name;
    private String href;
    private String img;
    private String updateTime;
    private Integer type;
    private Integer isUpdate;
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Videos() {
    }

    public Videos(String vid, String name, String href, String img, String updateTime, Integer type, Integer isUpdate, String source) {
        this.vid = vid;
        this.name = name;
        this.href = href;
        this.img = img;
        this.updateTime = updateTime;
        this.type = type;
        this.isUpdate = isUpdate;
        this.source = source;
    }

    @Override
    public String toString() {
        return "Videos{" +
                "vid='" + vid + '\'' +
                ", name='" + name + '\'' +
                ", href='" + href + '\'' +
                ", img='" + img + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", type=" + type +
                ", isUpdate=" + isUpdate +
                ", source='" + source + '\'' +
                '}';
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Integer isUpdate) {
        this.isUpdate = isUpdate;
    }
}
