package com.xh.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * @author：Zero度
 * @2020/11/14 18:07
 * 文件说明：文章表信息
 * */
public class Blog {
    /*
     * 文章id
     * return String
     * */
    @TableId(type = IdType.INPUT)
    private String articleId;

    /*
     * 发表用户id
     * return String
     * */
    private String userId;

    /*
     * 文章标题
     * return String
     * */
    private String articleTitle;

    /**
     * @creed: 简介
     * @return String
     */
    private String description;

    /*
     * 文章内容
     * return String
     * */
    private String articleContent;

    /**
     * 文章类型：
     * 11：java
     * 22：python
     * 33：linux
     * 44：mysql
     * 99：其他
     */
    private Integer articleType;

    /**
     * 文章状态：
     * 1：发布
     * 0：草稿
     */
    private Integer status;

    /**
     * 文章浏览量
     * return long
     * */
    private long articleViews;

    /**
     * 评论总数
     * return long
     * */
    private long articleComment_count;

    /**
     * 点赞数
     * return long
     * */
    private long articleLikeCount;

    /**
     *  发表时间
     * */
    private String articleDate;

    public Blog() {
    }

    public Blog(String articleId, String userId, String articleTitle, String description, String articleContent, Integer articleType, Integer status, long articleViews, long articleComment_count, long articleLikeCount, String articleDate) {
        this.articleId = articleId;
        this.userId = userId;
        this.articleTitle = articleTitle;
        this.description = description;
        this.articleContent = articleContent;
        this.articleType = articleType;
        this.status = status;
        this.articleViews = articleViews;
        this.articleComment_count = articleComment_count;
        this.articleLikeCount = articleLikeCount;
        this.articleDate = articleDate;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public Integer getArticleType() {
        return articleType;
    }

    public void setArticleType(Integer articleType) {
        this.articleType = articleType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public long getArticleViews() {
        return articleViews;
    }

    public void setArticleViews(long articleViews) {
        this.articleViews = articleViews;
    }

    public long getArticleComment_count() {
        return articleComment_count;
    }

    public void setArticleComment_count(long articleComment_count) {
        this.articleComment_count = articleComment_count;
    }

    public long getArticleLikeCount() {
        return articleLikeCount;
    }

    public void setArticleLikeCount(long articleLikeCount) {
        this.articleLikeCount = articleLikeCount;
    }

    public String getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(String articleDate) {
        this.articleDate = articleDate;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "articleId='" + articleId + '\'' +
                ", userId='" + userId + '\'' +
                ", articleTitle='" + articleTitle + '\'' +
                ", description='" + description + '\'' +
                ", articleContent='" + articleContent + '\'' +
                ", articleType=" + articleType +
                ", status=" + status +
                ", articleViews=" + articleViews +
                ", articleComment_count=" + articleComment_count +
                ", articleLikeCount=" + articleLikeCount +
                ", articleDate='" + articleDate + '\'' +
                '}';
    }
}