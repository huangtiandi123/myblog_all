package com.xh.pojo;

/*
 * @author：Zero度
 * @2020/12/8 9:45
 * 文件说明：评论分级处理
 * */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
// 忽略字段，否则报错
@JsonIgnoreProperties(value = { "handler" })
public class CommentDTO {
    private Integer commId;
    private String userName;
    private String content;
    private String date;
    private Integer userId;
    private String userUpName;
    private Integer commUpid;
//    private Comment comment;
    private List<CommentDTO> commentDTOList;

    public CommentDTO() {
    }

    public CommentDTO(int commId, String userName, String content, String date, int userId, String userUpName, int commUpid, List<CommentDTO> commentDTOList) {
        this.commId = commId;
        this.userName = userName;
        this.content = content;
        this.date = date;
        this.userId = userId;
        this.userUpName = userUpName;
        this.commUpid = commUpid;
        this.commentDTOList = commentDTOList;
    }

    public int getCommId() {
        return commId;
    }

    public void setCommId(int commId) {
        this.commId = commId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserUpName() {
        return userUpName;
    }

    public void setUserUpName(String userUpName) {
        this.userUpName = userUpName;
    }

    public int getCommUpid() {
        return commUpid;
    }

    public void setCommUpid(int commUpid) {
        this.commUpid = commUpid;
    }

    public List<CommentDTO> getCommentDTOList() {
        return commentDTOList;
    }

    public void setCommentDTOList(List<CommentDTO> commentDTOList) {
        this.commentDTOList = commentDTOList;
    }

    @Override
    public String toString() {
        return "CommentDTO{" +
                "commId=" + commId +
                ", userName='" + userName + '\'' +
                ", content='" + content + '\'' +
                ", date='" + date + '\'' +
                ", userId=" + userId +
                ", userUpName='" + userUpName + '\'' +
                ", commUpid=" + commUpid +
                ", commentDTOList=" + commentDTOList +
                '}';
    }
}
