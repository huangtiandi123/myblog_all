package com.xh.pojo;

/*
 * @author：Zero度
 * @2020/12/13 0:41
 * 文件说明：登录信息
 * */
public class LoginInfo {
    /**
     * 用户id
     */
    private String uid;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 角色
     */
    private String role;

    /**
     * token
     */
    private String token;

    public LoginInfo() {
    }

    public LoginInfo(String uid, String userName, String role, String token) {
        this.uid = uid;
        this.userName = userName;
        this.role = role;
        this.token = token;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginInfo{" +
                "uid='" + uid + '\'' +
                ", userName='" + userName + '\'' +
                ", role='" + role + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
