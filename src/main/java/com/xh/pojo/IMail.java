package com.xh.pojo;

import org.springframework.beans.factory.annotation.Value;

/*
 * @author：Zero度
 * @2020/12/26 16:20
 * 文件说明：
 * */
public class IMail {
    private String to;
    private String subject;
    private String content;
    private String upContent;
//    @Value("${spring.mail.from")
    private String from;

    public IMail() {
    }

    public IMail(String to, String subject, String content, String upContent, String from) {
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.upContent = upContent;
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUpContent() {
        return upContent;
    }

    public void setUpContent(String upContent) {
        this.upContent = upContent;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "IMail{" +
                "to='" + to + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", upContent='" + upContent + '\'' +
                ", from='" + from + '\'' +
                '}';
    }
}
