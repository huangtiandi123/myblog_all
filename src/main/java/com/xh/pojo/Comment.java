package com.xh.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/*
 * @author：Zero度
 * @2020/12/7 22:20
 * 文件说明：评论表信息
 * */
public class Comment {
    @TableId(value = "comm_id",type = IdType.AUTO)
    private Integer commId;
    private String articleId;
    private Integer userId;
    private Integer commUpId;
    private String userName;
    private String userUpName;
    private Integer status;
    private String content;
    private String date;

    public Comment() {
    }

    public Comment(Integer commId, String articleId, Integer userId, Integer commUpId, String userName, String userUpName, Integer status, String content, String date) {
        this.commId = commId;
        this.articleId = articleId;
        this.userId = userId;
        this.commUpId = commUpId;
        this.userName = userName;
        this.userUpName = userUpName;
        this.status = status;
        this.content = content;
        this.date = date;
    }

    public Integer getCommId() {
        return commId;
    }

    public void setCommId(Integer commId) {
        this.commId = commId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCommUpId() {
        return commUpId;
    }

    public void setCommUpId(Integer commUpId) {
        this.commUpId = commUpId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserUpName() {
        return userUpName;
    }

    public void setUserUpName(String userUpName) {
        this.userUpName = userUpName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commId=" + commId +
                ", articleId='" + articleId + '\'' +
                ", userId=" + userId +
                ", commUpId=" + commUpId +
                ", userName='" + userName + '\'' +
                ", userUpName='" + userUpName + '\'' +
                ", status=" + status +
                ", content='" + content + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
