package com.xh.pojo;

import springfox.documentation.spring.web.json.Json;

/*
 * @author：Zero度
 * @2020/11/14 18:08
 * 文件说明：
 * */
public class UserInfo {
    /**
     * @creed: 我的个人信息
     * @return Json
     */
    private Json myinfor;

    /**
     * @creed: 关于我
     * @return Json
     */
    private Json about;

    /**
     * @creed: 导航菜单
     * @return Json
     */
    private Json navmenu;

    public UserInfo() {
    }

    public UserInfo(Json myinfor, Json about, Json navmenu) {
        this.myinfor = myinfor;
        this.about = about;
        this.navmenu = navmenu;
    }

    public Json getMyinfor() {
        return myinfor;
    }

    public void setMyinfor(Json myinfor) {
        this.myinfor = myinfor;
    }

    public Json getAbout() {
        return about;
    }

    public void setAbout(Json about) {
        this.about = about;
    }

    public Json getNavmenu() {
        return navmenu;
    }

    public void setNavmenu(Json navmenu) {
        this.navmenu = navmenu;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "myinfor=" + myinfor +
                ", about=" + about +
                ", navmenu=" + navmenu +
                '}';
    }
}
