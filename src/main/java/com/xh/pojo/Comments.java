package com.xh.pojo;

/*
 * @author：Zero度
 * @2020/12/15 9:31
 * 文件说明：前端发送的评论数据包括ip、email
 * */
public class Comments {
    /**
     * 评论主体
     */
    private Comment comment;

    /**
     * 客户端ip
     */
    private String ip;

    /**
     * 评论者邮箱
     */
    private String email;

    public Comments() {
    }

    public Comments(Comment comment, String ip, String email) {
        this.comment = comment;
        this.ip = ip;
        this.email = email;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "comment=" + comment +
                ", ip='" + ip + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
