package com.xh.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/*
 * @author：Zero度
 * @2020/12/21 13:53
 * 文件说明：留言表信息
 * */
public class Leavmess {
    @TableId(type = IdType.INPUT)
    private Integer id;
    private String userName;
    private String email;
    private String content;
    private String leavtime;

    public Leavmess() {
    }

    public Leavmess(Integer id, String userName, String email, String content, String leavtime) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.content = content;
        this.leavtime = leavtime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLeavtime() {
        return leavtime;
    }

    public void setLeavtime(String leavtime) {
        this.leavtime = leavtime;
    }

    @Override
    public String toString() {
        return "Leavmess{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", content='" + content + '\'' +
                ", leavtime='" + leavtime + '\'' +
                '}';
    }
}
