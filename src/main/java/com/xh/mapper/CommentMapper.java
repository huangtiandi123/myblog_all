package com.xh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xh.pojo.Comment;
import com.xh.pojo.CommentDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2020/12/7 22:25
 * 文件说明：
 * */
@Repository
//@Mapper
public interface CommentMapper extends BaseMapper<Comment> {

    /**
     * @creed: 查询articleId的所有评论
     * @return List<CommentDTo></CommentDTo>
     */
    List<CommentDTO> getComments(@Param("articleId") String articleId,@Param("commUpId") int commUpId);

    /**
     * @creed: 添加评论
     * @return int 0 1
     */
    int addComment(Comment comment);
}
