package com.xh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xh.pojo.CtMg;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/*
 * @author：Zero度
 * @2020/12/20 21:25
 * 文件说明：
 * */
//@Mapper
@Repository
public interface CtMgMapper extends BaseMapper<CtMg> {
}
