package com.xh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xh.pojo.Blog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 * @author：Zero度
 * @2020/11/16 23:14
 * 文件说明：
 * */
@Repository
//@Mapper
public interface BlogMapper extends BaseMapper<Blog> {

    /**
     * 获取所有文章
     * @return
     */
    List<Blog> searchArticle();
}
