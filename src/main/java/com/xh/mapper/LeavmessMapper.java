package com.xh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xh.pojo.Leavmess;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/*
 * @author：Zero度
 * @2020/12/21 13:58
 * 文件说明：
 * */
@Repository
//@Mapper
public interface LeavmessMapper extends BaseMapper<Leavmess> {
}
