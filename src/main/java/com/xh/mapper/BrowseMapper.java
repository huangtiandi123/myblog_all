package com.xh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xh.pojo.Browse;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/*
 * @author：Zero度
 * @2021/1/22 17:40
 * 文件说明：
 * */
@Repository
//@Mapper
public interface BrowseMapper extends BaseMapper<Browse> {
}
