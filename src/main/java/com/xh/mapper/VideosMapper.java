package com.xh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xh.pojo.Videos;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/*
 * @author：Zero度
 * @2021/2/3 22:09
 * 文件说明：
 * */
@Repository
//@Mapper
public interface VideosMapper extends BaseMapper<Videos> {
}
