package com.xh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xh.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 * @author：Zero度
 * @2020/11/16 23:14
 * 文件说明：
 * */

@Repository
//@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * @creed: 根据userName查询登录者的信息进行认证授权
     * @return 用户信息对象
     */
    User selectUser(@Param("userName") String userName);
}
