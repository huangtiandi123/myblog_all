package com.xh.controller.api;

import com.xh.pojo.Comment;
import com.xh.pojo.CommentDTO;
import com.xh.pojo.Comments;
import com.xh.service.impl.CommentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2020/12/7 21:44
 * 文件说明：
 * */
@RestController
@RequestMapping("/api/comment/")
public class CommentController {

    @Autowired
    CommentServiceImpl commentServiceImpl;
    Map map;

    /**
     * 所有评论父子级处理输出
     * @param _t
     * @param articleId
     * @return
     */
    @RequestMapping(value = "list", produces = {"application/JSON;charset=utf-8"})
    public Map getComment(@RequestParam String _t, @RequestParam String articleId){
        List<CommentDTO> comments = commentServiceImpl.getComments(articleId);

        map = new HashMap();

        map.put("code", 200);
        map.put("data", comments);
        return map;
    }

    /**
     * 发表评论
     * @param comments
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.POST, produces = {"application/JSON;charset=utf-8"})
    public Map addComment(@RequestBody Comments comments){
        int i = commentServiceImpl.addComment(comments.getComment());

        String message = null;
        int code = 0;
        if (i > 0){
            message = "评论发表成功";
            code = 200;
        }else{
            message = "评论发表失败";
            code = 0;
        }

        map = new HashMap();
        map.put("code", code);
        map.put("message",message);
        return map;
    }

    @GetMapping(value = "allunrevieded", produces = {"application/JSON;charset=utf-8"})
    public Map unComment(){
        List<Comment> commentList = commentServiceImpl.selectall();
        int code = 0;
        if(!commentList.isEmpty()){
            code = 200;
        }
        map = new HashMap();
        map.put("code", code);
        map.put("data", commentList);
        return map;
    }

    /**
     * 审核操作
     * @param comment
     * @return
     */
    @PostMapping(value = "checkoper", produces = {"application/JSON;charset=utf-8"})
    public Map checkComment(@RequestBody Comment comment){
        System.out.println("commid===" + comment.getCommId() + "status===" + comment.getStatus());
        int i = commentServiceImpl.checkComment(comment.getCommId(), comment.getStatus());

        String message = "审核成功";
        map = new HashMap();
        map.put("code", 200);
        map.put("message",message);

        return map;
    }
}
