package com.xh.controller.api;

/*
 * @author：Zero度
 * @2020/12/21 13:55
 * 文件说明：
 * */

import com.xh.pojo.IMail;
import com.xh.pojo.Leavmess;
import com.xh.service.impl.IMailServiceImpl;
import com.xh.service.impl.LeavmessServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/")
public class LeavmessController {

    @Autowired
    LeavmessServiceImpl leavmessServiceImpl;
    @Autowired
    IMailServiceImpl iMailServiceImpl;

    Map map;

    @GetMapping("mess/list")
    public Map getLeavmess(){
        List<Leavmess> allLeavmess = leavmessServiceImpl.getAllLeavmess();

        map = new HashMap();
        map.put("code", 200);
        map.put("data", allLeavmess);
        return map;
    }

    @RequestMapping("mess/add")
    public Map addMess(@RequestBody Leavmess leavmess){
        int i = leavmessServiceImpl.addMess(leavmess);
        String message = null;
        if(i == 1){
            message = "留言发表成功";
        }else{
            message = "留言发表失败";
        }
        map = new HashMap();
        map.put("code", 200);
        map.put("message", message);
        return map;
    }

    /**
     * 发送邮件
     * @param iMail
     * @return
     */
    @RequestMapping("mess/sendmail")
    public Map sendSimpMail(@RequestBody IMail iMail){
        iMail.setFrom("xiaobei15@163.com");//"qweasd2584@qq.com");
        String message = iMailServiceImpl.sendSimpHtml(iMail);

        map = new HashMap();
        map.put("code", 200);
        map.put("message", message);
        return map;
    }
}
