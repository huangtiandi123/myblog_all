package com.xh.controller.api;

/*
 * @author：Zero度
 * @2020/11/16 23:17
 * 文件说明：后台博客控制层
 * */

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xh.pojo.Blog;
import com.xh.service.impl.ArticleServiceImpl;
import com.xh.service.impl.BlogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/admin/api/")
public class BlogController {

    @Autowired
    BlogServiceImpl blogServiceImpl;
    @Autowired
    ArticleServiceImpl articleServiceImpl;

    private static Map map;
    private static String message;
    private static Integer code;

    @RequestMapping(value = "article/list", produces = {"application/JSON;charset=utf-8"})
    public Map getAllArticle(@RequestParam String _t, @RequestParam(defaultValue = "1") Integer p,
                             @RequestParam Integer size) {
            Page<Blog> blogPage = new Page<>(p,size);
            Map blogsMap = blogServiceImpl.allArticle(blogPage);
            map = new HashMap();
            map.put("data",blogsMap.get("data"));
            map.put("total",blogsMap.get("total"));
        map.put("code", 200);
        return map;
    }

    /**
     * 前端请求添加文章控制
     * @param blog
     * @return
     */
    @RequestMapping(value = "addarticle", method = RequestMethod.POST, produces = {"application/JSON;charset=utf-8"})
    public Map addArticle(@RequestBody Blog blog){
        // 生成七位的随机文章id
        String articleId = RandomUtil.randomString(7);
        blog.setArticleId(articleId);
        blog.setUserId("1001");
        int i = blogServiceImpl.addArticle(blog);
        if(i>0){
            System.out.println("-------记录添加成功-----");
        }

        map.put("code", 200);
        map.put("message","文章添加成功");
        return map;
    }

    @RequestMapping(value = "updatearticle" , method = RequestMethod.POST, produces = {"application/JSON;charset=utf-8"})
    public Map updateArticle(@RequestBody Blog blog){
        System.out.println(blog.toString());

        map.put("code", 200);
        return map;
    }

    @RequestMapping(value = "deletearticle" , method = RequestMethod.POST, produces = {"application/JSON;charset=utf-8"})
    public Map deleteArticle(@RequestBody Blog blog){
        String articleId = blog.getArticleId();
        System.out.println(articleId);
        int i = blogServiceImpl.delArticle(articleId);
        if(i == 1) {
            message = "数据库指定记录删除成功";
        }else {
            message = "数据库指定记录删除失败";
        }
        map.put("message", message);
        map.put("code", 200);
        return map;
    }

    @RequestMapping(value = "modifyStatus", method = RequestMethod.POST, produces = {"application/JSON;charset=utf-8"})
    public Map modifyStatus(@RequestBody Blog blog){

        int i = blogServiceImpl.updateStatus(blog);
        if (i == 1) {
            code = 200;
            message = "发布成功";
        }else {
            code = 300;
            message = "发布失败";
        }
        map = new HashMap();
        map.put("code", code);
        map.put("message", message);

        return map;
    }
}
