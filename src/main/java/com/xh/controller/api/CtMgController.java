package com.xh.controller.api;

import com.xh.service.impl.CtMgServiceImpl;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2020/12/20 21:21
 * 文件说明：网站各类菜单、列表等信息控制层
 * */
@RestController
@RequestMapping("/api/")
public class CtMgController {

    @Autowired
    CtMgServiceImpl ctMgServiceImpl;

    Map map;
    /**
     * 获取分类列表菜单
     * @return
     */
    @RequestMapping("cate/list")
    public Map getCate(@RequestParam String _t){
        List cateList = ctMgServiceImpl.getCateList();

        map = new HashMap();

        map.put("code", 200);
        map.put("message", "获取分类成功");
        map.put("data", cateList);
        return map;
    }
}
