package com.xh.controller.api;

import cn.hutool.aop.interceptor.SpringCglibInterceptor;
import com.xh.pojo.Videos;
import com.xh.service.impl.VideosServiceImpl;
import org.apache.coyote.http11.upgrade.UpgradeServletOutputStream;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2021/2/3 21:56
 * 文件说明：
 * */
@RestController
@RequestMapping("/api/videos/")
public class VideosController {

    @Autowired
    VideosServiceImpl videosServiceImpl;
    Map<String, Object> map;

    @RequestMapping("list")
    public Map<String, Object> getVideosList(){

        List videosList = videosServiceImpl.selectVideosList();
        map = new HashMap<>();
        map.put("data", videosList);
        map.put("code", 200);
        return map;
    }

    @RequestMapping("add")
    public Map<String, Object> addVideo(@RequestBody Videos videos){

        System.out.println("-------/n" + videos.toString());

        map = new HashMap<>();
        map.put("code", 200);
        return map;
    }
}
