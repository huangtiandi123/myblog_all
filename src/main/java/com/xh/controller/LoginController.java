package com.xh.controller;

/*
 * @author：Zero度
 * @2020/11/16 23:17
 * 文件说明：
 * */

import com.xh.pojo.Result;
import com.xh.pojo.User;
import com.xh.service.impl.LoginServiceImpl;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/admin/")
public class LoginController {

    @Autowired
    LoginServiceImpl loginServiceImpl;

    Map<String, Object> map;

    @RequestMapping(value = "login", method = RequestMethod.POST, produces = {"application/JSON;charset=utf-8"})
    public Result login(@RequestBody User user, HttpServletRequest request){

        // 进行登录认证
        Result result = loginServiceImpl.selectUser(user);

//        String path = request.getContextPath();
//        Enumeration<String> headerNames = request.getHeaderNames();
//        System.out.println(headerNames.toString());
//        System.out.println("地址："+ path);

        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("user-agent"));
        String clientType = userAgent.getOperatingSystem().getDeviceType().toString();
        System.out.println("客户端类型：" + clientType);
        String name = userAgent.getOperatingSystem().getName();
        System.out.println("操作系统类型：" + name);
        String browserType = userAgent.getBrowser().toString();
        System.out.println("浏览器类型: " + browserType);
        String browserV = userAgent.getBrowserVersion().toString();
        System.out.println("浏览器版本：" + browserV);
        String ipAddr = getIpAddr(request);
        System.out.println("ip地址：" + ipAddr);

        return result;
    }

    //其中ip的获取方式
    public static String getIpAddr(HttpServletRequest request) {

        String ip = request.getHeader("x-forwarded-for");

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
            System.out.println("X-Real-IP:" + ip);
            //LOGGER.error("X-Real-IP:"+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("http_client_ip");
            System.out.println("http_client_ip:"+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            System.out.println("getRemoteAddr:"+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
            System.out.println("Proxy-Client-IP:"+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
            System.out.println("WL-Proxy-Client-IP:"+ip);
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            System.out.println("HTTP_X_FORWARDED_FOR:"+ip);
        }
            // 如果是多级代理，那么取第一个ip为客户ip
        if (ip != null && ip.indexOf(",") != -1) {
            ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
            //            LOGGER.error("ip:"+ip);
        }
        return ip;
    }
}
