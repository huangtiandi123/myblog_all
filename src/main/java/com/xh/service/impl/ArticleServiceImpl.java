package com.xh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.mapper.BlogMapper;
import com.xh.pojo.Blog;
import com.xh.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2020/11/23 9:47
 * 文件说明：文章业务层
 * */
@Service
public class ArticleServiceImpl extends ServiceImpl<BlogMapper, Blog> implements ArticleService{

    @Autowired
    BlogMapper blogMapper;

    /**
     * 获取所有文章
     * @param blogPage
     * @return
     */
    @Override
    public Map allArticle(Page blogPage) {
        // 排序分页查询
        blogPage.setDescs(Arrays.asList(new String[] {"article_date"}));
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("status",1);
        IPage blogIPage = blogMapper.selectPage(blogPage, wrapper);
        List<Blog> blogs = blogIPage.getRecords();
        long total = blogIPage.getTotal();
        Map map = new HashMap();
        map.put("data", blogs);
        map.put("total", total);
        return map;

    }

    /**
     * 获取分类文章
     * @param cate_id
     * @param blogPage
     * @return
     */
    @Override
    public Map getCateArticle(Integer cate_id, Page blogPage) {
        blogPage.setDescs(Arrays.asList(new String[] {"article_date"}));
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("article_type", cate_id);
        IPage blogIPage = blogMapper.selectPage(blogPage, wrapper);
        List<Blog> blogs = blogIPage.getRecords();
        long total = blogIPage.getTotal();
        Map map = new HashMap();
        map.put("data", blogs);
        map.put("total", total);
        return map;
    }
}
