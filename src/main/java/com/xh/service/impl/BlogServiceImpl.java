package com.xh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.mapper.BlogMapper;
import com.xh.pojo.Blog;
import com.xh.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2020/12/1 18:09
 * 文件说明：
 * */

@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements BlogService {

    @Autowired
    BlogMapper blogMapper;

    /**
     * 获取所有文章
     * @param blogPage
     * @return
     */
    @Override
    public Map allArticle(Page blogPage) {
        // 排序分页查询
        blogPage.setDescs(Arrays.asList(new String[] {"article_date"}));
//        QueryWrapper wrapper = new QueryWrapper();
        IPage blogIPage = blogMapper.selectPage(blogPage, null);
        List<Blog> blogs = blogIPage.getRecords();
        long total = blogIPage.getTotal();
        Map map = new HashMap();
        map.put("data", blogs);
        map.put("total", total);
        return map;

    }

    /**
     * 添加博客文章
     * @param blog
     * @return
     */
    @Override
    public int addArticle(Blog blog) {
        int insert = blogMapper.insert(blog);
        return insert;
    }

    @Override
    public int delArticle(String articleId) {
//        QueryWrapper wrapper = new QueryWrapper();
//        wrapper.select("article_id",articleId);
        int i = blogMapper.deleteById(articleId);
        return i;
    }

    @Override
    public int updateStatus(Blog blog){
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("article_id", blog.getArticleId());
        blog.setStatus(1);
        int update = blogMapper.update(blog, wrapper);
        return update;
    }

}

