package com.xh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.mapper.VideosMapper;
import com.xh.pojo.Videos;
import com.xh.service.VideosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2021/2/3 22:07
 * 文件说明：
 * */
@Service
public class VideosServiceImpl extends ServiceImpl<VideosMapper, Videos> implements VideosService {

    @Autowired
    VideosMapper videosMapper;

    @Override
    public List<Videos> selectVideosList() {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.select("*");
        Map map = new HashMap();
        List videosList = videosMapper.selectList(wrapper);
        return videosList;
    }
}
