package com.xh.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.mapper.LeavmessMapper;
import com.xh.pojo.Leavmess;
import com.xh.service.LeavmessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/*
 * @author：Zero度
 * @2020/12/21 13:56
 * 文件说明：
 * */
@Service
public class LeavmessServiceImpl extends ServiceImpl<LeavmessMapper, Leavmess> implements LeavmessService {

    @Autowired
    LeavmessMapper leavmessMapper;


    @Override
    public List<Leavmess> getAllLeavmess() {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.select("*");
        List list = leavmessMapper.selectList(wrapper);
        return list;
    }

    /**
     * 添加留言
     * @param leavmess
     * @return
     */
    @Override
    public int addMess(Leavmess leavmess) {
//        int id = RandomUtil.randomInt(11000, 99999);
//        leavmess.setId(id);
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String createdate = simpleDateFormat.format(date);
        leavmess.setLeavtime(createdate);
        int insert = leavmessMapper.insert(leavmess);
        return insert;
    }
}
