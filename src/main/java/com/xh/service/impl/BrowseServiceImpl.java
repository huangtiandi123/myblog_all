package com.xh.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.mapper.BrowseMapper;
import com.xh.pojo.Browse;
import org.springframework.stereotype.Service;

/*
 * @author：Zero度
 * @2021/1/22 17:39
 * 文件说明：
 * */
@Service
public class BrowseServiceImpl extends ServiceImpl<BrowseMapper, Browse> {

}
