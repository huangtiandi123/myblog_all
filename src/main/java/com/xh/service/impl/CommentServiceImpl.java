package com.xh.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.mapper.CommentMapper;
import com.xh.pojo.Comment;
import com.xh.pojo.CommentDTO;
import com.xh.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * @author：Zero度
 * @2020/12/7 22:27
 * 文件说明：评论服务层
 * */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Autowired
    CommentMapper commentMapper;

    /**
     * 查询并处理查询结果的评论多层嵌套，转为两层
     * @creed: articleId
     * @return 处理后的两层评论列表
     */
    @Override
    public List<CommentDTO> getComments(String articleId) {

        List<CommentDTO> comments = commentMapper.getComments(articleId, 0);
        System.out.println(comments);
        List<CommentDTO> parent = findParent(comments);
        return parent;
    }

    /**
     * 处理评论
     * @param comments
     * @return 处理好的两层评论
     */
    public List<CommentDTO> findParent(List<CommentDTO> comments){

        // 防止checkForComodification(), 而建立一个新集合
        for (CommentDTO comment : comments) {
            ArrayList<CommentDTO> fatherComment = new ArrayList<>();
            // 递归处理子级的回复，及回复内有回复
            findComment(comment, fatherComment);

            // 递归处理后的集合放回父级的子级中
            comment.setCommentDTOList(fatherComment);
        }

        return comments;
    }

    /**
     * 处理评论子级
     * @param parent
     * @param fatherComment
     */
    private void findComment(CommentDTO parent, ArrayList<CommentDTO> fatherComment) {

        // 找出直接子级
        List<CommentDTO> comments = parent.getCommentDTOList(); // 正常

        // 遍历直接子级的子级
        for (CommentDTO comment : comments) {

            // 若非空，则还有子级，递归
            if(!comment.getCommentDTOList().isEmpty()){
                findComment(comment,fatherComment);
            }
            // 已经到了最底层的嵌套关系，奖该回复放入新建立的集合
            fatherComment.add(comment);

            // 容易忽略的地方： 将相对底层的子级放入新建立的集合之后
            // 则表示解除了嵌套关系，对应的其父级的子级应该设为空
            comment.setCommentDTOList(new ArrayList<>());
        }
    }

    /**
     * 添加评论
     * @param comment
     * @return
     */
    @Override
    public int addComment(Comment comment) {

        // 评论的时间
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String createdate =simpleDateFormat.format(date);
        comment.setDate(createdate);
        if(comment.getUserName().equals("")){
            comment.setUserName("匿名");
        }
        int i = RandomUtil.randomInt(1000,9999);
        comment.setUserId(i);
        int insert = commentMapper.insert(comment);

        return insert;
    }


    /**
     * 审核操作修改status  0 未审核 1 通过 -1 不通过
     * @param status
     * @return
     */
    @Override
    public int checkComment(Integer commId,Integer status) {
        switch (status){
            case 1:
                Comment comment = new Comment();
                comment.setCommId(commId);
                comment.setStatus(status);
                int upI = commentMapper.updateById(comment);
                return upI;
            case -1:
                int delI = commentMapper.deleteById(commId);
                return delI;
            default:
                return -1;
        }
    }

    /**
     * 查询出所有未审核的评论 status==0
     * @return
     */
    @Override
    public List<Comment> selectall() {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("status", 0);
        List commentList = commentMapper.selectList(wrapper);
        return commentList;
    }
}
