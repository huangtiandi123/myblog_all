package com.xh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xh.mapper.CtMgMapper;
import com.xh.pojo.CtMg;
import com.xh.service.CtMgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/*
 * @author：Zero度
 * @2020/12/20 21:25
 * 文件说明：
 * */
@Service
public class CtMgServiceImpl extends ServiceImpl<CtMgMapper, CtMg> implements CtMgService {

    @Autowired
    CtMgMapper ctMgMapper;

    /**
     * 获取分类列表菜单
     * @return
     */
    @Override
    public List getCateList() {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.isNotNull("cate_name");
        List list = ctMgMapper.selectList(wrapper);

        return list;
    }
}
