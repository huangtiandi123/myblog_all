package com.xh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.Leavmess;

import java.util.List;

/*
 * @author：Zero度
 * @2020/12/21 14:01
 * 文件说明：
 * */
public interface LeavmessService extends IService<Leavmess> {

    List<Leavmess> getAllLeavmess();

    int addMess(Leavmess leavmess);

}
