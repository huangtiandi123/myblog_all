package com.xh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.Videos;

import java.util.List;

/*
 * @author：Zero度
 * @2021/2/3 22:13
 * 文件说明：
 * */
public interface VideosService extends IService<Videos> {
    List selectVideosList();
}
