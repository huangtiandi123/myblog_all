package com.xh.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.Blog;
import java.util.Map;

/*
 * @author：Zero度
 * @2020/12/14 22:40
 * 文件说明：
 * */
public interface BlogService extends IService<Blog> {
    Map allArticle(Page blogPage);

    int addArticle(Blog blog);

    int delArticle(String articleId);

    int updateStatus(Blog blog);
}
