package com.xh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.Browse;
import org.springframework.stereotype.Service;

/*
 * @author：Zero度
 * @2021/1/22 17:44
 * 文件说明：
 * */
public interface BrowseService extends IService<Browse> {

}
