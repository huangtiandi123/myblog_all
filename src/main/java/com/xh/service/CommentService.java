package com.xh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.Comment;
import com.xh.pojo.CommentDTO;

import java.util.List;

/*
 * @author：Zero度
 * @2020/12/9 13:18
 * 文件说明：
 * */
public interface CommentService extends IService<Comment> {
    List<CommentDTO> getComments(String articleId);
    // 添加评论
    int addComment(Comment comment);

    // 更新status或删除
    int checkComment(Integer commId,Integer status);

    // 查询所有未审核的评论
    List<Comment> selectall();
}
