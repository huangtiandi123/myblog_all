package com.xh.service;

import com.xh.pojo.IMail;

/*
 * @author：Zero度
 * @2020/12/26 16:12
 * 文件说明：邮件发送类型接口
 * */
public interface IMailService {

    /**
     * 发送文本邮件
     * @param iMail
     */
    void sendSimpMail(IMail iMail);

    /**
     * 发送Html邮件
     * @param iMail
     */
    String sendSimpHtml(IMail iMail);
}
