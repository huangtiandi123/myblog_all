package com.xh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.Result;
import com.xh.pojo.User;

/*
 * @author：Zero度
 * @2020/12/10 22:57
 * 文件说明：
 * */

public interface UserService extends IService<User> {
    /**
     * 查询用户登录信息
     * @param user
     * @return
     */
    Result selectUser(User user);
}
