package com.xh.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.Blog;

import java.util.Map;

/*
 * @author：Zero度
 * @2020/11/22 23:10
 * 文件说明：文章业务层接口
 * */
public interface ArticleService extends IService<Blog> {
    /**
     * 分页获取文章
     * @param blogPage
     * @return
     */
    Map allArticle(Page blogPage);

    Map getCateArticle(Integer cate_id, Page blogPage);
}