package com.xh.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xh.pojo.CtMg;

import java.util.List;

/*
 * @author：Zero度
 * @2020/12/20 21:24
 * 文件说明：
 * */
public interface CtMgService extends IService<CtMg> {

    /**
     * 获取分类列表
     * @return
     */
    List getCateList();
}
