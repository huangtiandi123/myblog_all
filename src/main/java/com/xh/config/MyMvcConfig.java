package com.xh.config;

import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
 * @author：Zero度
 * @2020/11/16 23:20
 * 文件说明：生成配置
 * */
public class MyMvcConfig implements WebMvcConfigurer {

    /**
     * @return void
     * @creed: 视图跳转
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
    }

}
