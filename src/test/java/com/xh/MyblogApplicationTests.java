package com.xh;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MyblogApplicationTests {

    @Test
    void contextLoads() {
//        SqlSession sqlSession = MybatisUtil.getSqlSession();
//        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
//        List<Blog> blogList = mapper.searchArticle();
//        for (Blog blog : blogList) {
//            System.out.println(blog);
//        }
//        sqlSession.close();


    }

    @Test
    void md5Test() {
        String hashAlgoritName = "MD5";
        String password = "xh199915";
        int hashIterations = 1024;
        // 使用了盐值加密
        ByteSource byteSource = ByteSource.Util.bytes("xh9159");
        Object obj = new SimpleHash(hashAlgoritName, password, byteSource, hashIterations);
        System.out.println(obj);
    }

//    @Autowired
//    LoginServiceImpl loginServiceImpl;
//
//    @Test
//    void dataTest(){
//        User user = new User();
//        user.setUserName("xiaohe");
//        user.setPassword("123456");
//        loginServiceImpl.selectUser(user);
//    }

}
